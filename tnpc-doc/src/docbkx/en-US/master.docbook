<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book PUBLIC '-//OASIS//DTD DocBook XML V5.0//EN' '../../../target/classes/xsd/docbook.dtd' [
        <!ENTITY introGeneralities SYSTEM "generalities/introduction.docbook">
        <!ENTITY pictureDatabase SYSTEM "database/database.docbook">
        <!ENTITY acquisition SYSTEM "acquisition/acquisition.docbook">
        <!ENTITY modeLive SYSTEM "modeLive/modeLive.docbook">
        <!ENTITY annotation SYSTEM "annotation/annotation.docbook">
        <!ENTITY shapeAnalysis SYSTEM "shapeAnalysis/shapeAnalysis.docbook">
        <!ENTITY automaticEstimation SYSTEM "automaticEstimation/automaticEstimation.docbook">
        <!ENTITY exportFile SYSTEM "export/export.docbook">
        <!ENTITY glossary SYSTEM "glossary.docbook">
        <!ENTITY extendTnpc SYSTEM "extendTnpc/extendTnpc.docbook">
        <!ENTITY cameras SYSTEM "supportedCameras.docbook">
        ]
>

<book>

    <info>
        <copyright>
            <year>2010-2011</year>
            <holder>Ifremer, Code Lutin, Jean Couteau, Noesis SA</holder>
        </copyright>

    </info>

    <bookinfo>
        <title>TNPC</title>
        <subtitle>User guide</subtitle>
        <releaseinfo>5.0</releaseinfo>
        <authorgroup>
            <author>
                <logo-fileref>images/logo/LogoPoleScleroRVBAnglais.jpg</logo-fileref>
                <role>Scientific Coordinator</role>
                <firstname>Kélig</firstname>
                <surname>Mahé</surname>
                <affiliation>
                    <orgname>IREMER Institut, Fisheries laboratory,
                        Sclerochronology centre</orgname>
                </affiliation>
                <email>kelig.mahe@ifremer.fr</email>
            </author>
            <author>
                <logo-fileref>images/logo/logo_noesis.jpg</logo-fileref>
                <role>Software Coordinator</role>
                <firstname>Sébastien</firstname>
                <surname>Fave</surname>
                <affiliation><orgname>Noesis</orgname></affiliation>
                <email>sf@noesis.fr</email>
            </author>
            <author>
                <logo-fileref>images/logo/CodelutinOrange.png</logo-fileref>
                <role>User Guide Coordinator</role>
                <firstname>Jean</firstname>
                <surname>Couteau</surname>
                <affiliation><orgname>Code Lutin</orgname></affiliation>
                <email>couteau@codelutin.com</email>
            </author>
        </authorgroup>

        <legalnotice>
            <title>Legal Notice - License Creative Commons - By-SA</title>

            <para><emphasis role="bold">You are free :</emphasis></para>

            <itemizedlist>
                <listitem>
                    <para>
                        <emphasis role="bold">to Share</emphasis> - to copy,
                        distribute and transmit the work
                    </para>
                </listitem>
                <listitem>
                    <para>
                        <emphasis role="bold">to Remix</emphasis> - to adapt the
                        work
                    </para>
                </listitem>
            </itemizedlist>

            <para>
                <emphasis role="bold">Under the following conditions :</emphasis>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        <emphasis role="bold">Attribution</emphasis>
                        — You must attribute the work in the manner specified by
                        the author or licensor (but not in any way that suggests
                        that they endorse you or your use of the work).
                    </para>
                </listitem>

                <listitem>
                    <para>
                        <emphasis role="bold">Share Alike</emphasis> — If you
                        alter, transform, or build upon this work, you may
                        distribute the resulting work only under the same or
                        similar license to this one.
                    </para>
                </listitem>

            </itemizedlist>

            <para>
                <emphasis role="bold">With the understanding that:</emphasis>
            </para>

            <itemizedlist>
                <listitem>
                    <para>
                        <emphasis role="bold">Waiver</emphasis> - Any of the
                        above conditions can be
                        <emphasis role="bold">waived</emphasis> if you get
                        permission from the copyright holder.
                    </para>
                </listitem>

                <listitem>
                    <para>
                        <emphasis role="bold">Public Domain</emphasis> - Where
                        the work or any of its elements is in the
                        <emphasis role="bold">public domain</emphasis>
                        under applicable law, that status is in no way affected
                        by the license.
                    </para>
                </listitem>

                <listitem>
                    <para>
                        <emphasis role="bold">Other Rights</emphasis> - In no
                        way are any of the following rights affected by the
                        license:
                    </para>

                    <itemizedlist>
                        <listitem>
                            <para>
                                Your fair dealing or
                                <emphasis role="bold">fair use</emphasis>
                                rights, or other applicable copyright exceptions
                                and limitations;
                            </para>
                        </listitem>

                        <listitem>
                            <para>
                                The author's
                                <emphasis role="bold">moral rights</emphasis>;
                            </para>
                        </listitem>

                        <listitem>
                            <para>
                                Rights other persons may have either in the work
                                itself or in how the work is used, such as
                                <emphasis role="bold">publicity</emphasis> or
                                privacy rights.
                            </para>
                        </listitem>

                    </itemizedlist>

                </listitem>

                <listitem>
                    <para>
                        <emphasis role="bold">Notice</emphasis> - For any reuse
                        or distribution, you must make clear to others the
                        license terms of this work. The best way to do this is
                        with a link to
                        <ulink url="http://creativecommons.org/licenses/by-sa/3.0/">
                            this web page
                        </ulink>.
                    </para>
                </listitem>
            </itemizedlist>

            <para>
                The full text of the license can be found on
                <ulink url="http://creativecommons.org/licenses/by-sa/3.0/legalcode">
                    the Creative Commons website
                </ulink>
            </para>
        </legalnotice>

        <mediaobject>
            <imageobject>
                <imagedata fileref="images/logo/logo_TNPC.png"/>
            </imageobject>
        </mediaobject>

    </bookinfo>

    &introGeneralities;
    &pictureDatabase;
    &acquisition;
    &annotation;
    &shapeAnalysis;
    &modeLive;
    &automaticEstimation;
    &extendTnpc;
    &exportFile;
    &cameras;
    <index/>
    &glossary;

</book>
