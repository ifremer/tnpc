<?xml version='1.0'?>
<!--
  #%L
  TNPC documentation :: XSL Transformations
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2010 Ifremer, CodeLutin, Noesis, JBoss, a division
              of Red Hat
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->


<!--
    Copyright 2009-2010 Ifremer, Code Lutin, Jean Couteau
    License: GPL v3
    Author: Jean Couteau (couteau@codelutin.com)
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="http://www.w3.org/TR/xhtml1/transitional"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:date="http://exslt.org/dates-and-times">

    <!-- Use xsl transfo extracted from dependency instead of maven plugin
    wrapper for correct callouts generation -->
    <xsl:import href="docbook/fo/docbook.xsl"/>

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <!--Numbering-->
    <xsl:param name="appendix.autolabel" select="'A'"/>
    <xsl:param name="chapter.autolabel" select="1"/>
    <xsl:param name="part.autolabel" select="'I'"/>
    <xsl:param name="reference.autolabel" select="'I'"/>
    <xsl:param name="section.autolabel" select="1"/>
    <xsl:param name="section.label.includes.component.label" select="1"/>

    <!-- No intendation of Titles -->
    <xsl:param name="body.start.indent">0pt</xsl:param>

    <!--Text format-->
    <xsl:param name="alignment">justify</xsl:param>
    <xsl:param name="line-height" select="1.5"/>

    <!--Page format-->
    <xsl:param name="paper.type" select="'A4'"/>

    <!--Title colors-->
    <xsl:param name="title.color">#6161e1</xsl:param>
    <xsl:param name="chapter.title.color" select="$title.color"/>
    <xsl:param name="section.title.color" select="$title.color"/>
    <xsl:param name="titlepage.color" select="$title.color"/>

    <!--Title page properties-->
    <xsl:attribute-set name="book.titlepage.recto.style">
        <xsl:attribute name="color">
            <xsl:value-of select="$chapter.title.color"/>
        </xsl:attribute>
        <xsl:attribute name="background-color">white</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="padding-left">1em</xsl:attribute>
        <xsl:attribute name="padding-right">1em</xsl:attribute>
    </xsl:attribute-set>

    <!-- Define the margins, background image for the cover page -->
    <xsl:template name="user.pagemasters">

        <fo:page-sequence-master master-name="coversequence">
            <fo:repeatable-page-master-alternatives>
                <fo:conditional-page-master-reference
                        master-reference="titlepage-first-ifremer"
                        page-position="first"/>
                <fo:conditional-page-master-reference
                        master-reference="titlepage-odd-ifremer"
                        odd-or-even="odd"/>
                <fo:conditional-page-master-reference
                        master-reference="titlepage-even-ifremer"
                        odd-or-even="even"/>
                <fo:conditional-page-master-reference
                        master-reference="blank"
                        blank-or-not-blank="blank"/>
            </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>

        <fo:simple-page-master master-name="titlepage-first-ifremer"
                               page-width="210mm"
                               page-height="297mm"
                               margin-top="0in"
                               margin-bottom="0in"
                               margin-left="0in"
                               margin-right="0in">
            <fo:region-body margin-bottom="0.0in"
                            margin-top="0.0in"
                            margin-left="0.0in"
                            margin-right="0.0in"
                            column-gap="0pt"
                            column-count="1"
                            background-repeat="no-repeat">
                <xsl:attribute name="background-image">
                    <xsl:if test="$img.src.path != ''">
                        <xsl:value-of select="$img.src.path"/>
                    </xsl:if>
                    <xsl:text>images/frontcover.png</xsl:text>
                </xsl:attribute>
            </fo:region-body>
        </fo:simple-page-master>

        <fo:simple-page-master master-name="titlepage-odd-ifremer"
                               page-width="210mm"
                               page-height="297mm"
                               margin-top="0in"
                               margin-bottom="0in"
                               margin-left="0in"
                               margin-right="0in">
            <fo:region-body margin-bottom="0.5in"
                            margin-top="0.5in"
                            margin-left="0.5in"
                            margin-right="0.5in"
                            column-gap="0pt"
                            column-count="1">
            </fo:region-body>
        </fo:simple-page-master>

        <fo:simple-page-master master-name="titlepage-even-ifremer"
                               page-width="210mm"
                               page-height="297mm"
                               margin-top="0in"
                               margin-bottom="0in"
                               margin-left="0in"
                               margin-right="0in">
            <fo:region-body margin-bottom="0.5in"
                            margin-top="0.5in"
                            margin-left="0.5in"
                            margin-right="0.5in"
                            column-gap="0pt"
                            column-count="1">
            </fo:region-body>
        </fo:simple-page-master>
    </xsl:template>

    <!-- Use custom pagemaster for cover pages -->
    <xsl:template name="select.user.pagemaster">
        <xsl:param name="element"/>
        <xsl:param name="pageclass"/>
        <xsl:param name="default-pagemaster"/>
        <xsl:choose>
            <xsl:when test="$element = 'book' and $pageclass = 'titlepage'">
                <xsl:value-of select="'coversequence'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$default-pagemaster"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


        <!-- Recto cover page. -->
    <xsl:template name="book.titlepage.recto">

        <!--authors-->
        <!--fo:block margin-left="64mm"
                  margin-top="10mm"-->
            <fo:table table-layout="fixed" margin-left="40mm">
                <fo:table-column column-width="13mm"/>
                <fo:table-column column-width="150mm"/>
                <fo:table-body>

                    <xsl:for-each select="/book/bookinfo/authorgroup/author">
                        <!--empty row to make some space-->
                        <fo:table-row height="4mm">
                            <fo:table-cell><fo:block/></fo:table-cell>
                            <fo:table-cell><fo:block/></fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                            <fo:table-cell display-align="center">
                                <!--logo-->
                                <fo:block text-align="center">
                                    <fo:external-graphic content-width="23mm"
                                    content-height="23mm">
                                        <xsl:attribute name="src">
                                            <xsl:if test="$img.src.path != ''">
                                                <xsl:value-of
                                                select="$img.src.path"/>
                                            </xsl:if>
                                            <xsl:value-of select="logo-fileref"/>
                                        </xsl:attribute>
                                    </fo:external-graphic>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <!--Role-->
                                <fo:block font-weight="bold">
                                    <xsl:value-of select="role"/>
                                </fo:block>
                                <!-- Name -->
                                <fo:block>
                                    <xsl:value-of select="firstname"/>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="surname"/>
                                </fo:block>
                                <!-- Organisation -->
                                <fo:block>
                                    <xsl:value-of select="affiliation/orgname"/>
                                </fo:block>
                                <!-- Email -->
                                <fo:block>
                                    <xsl:value-of select="email"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        <!--/fo:block-->


        <!-- infos-->
        <!--xsl:variable name="now" select="date:date-time()"/>
        <fo:block margin-left="64mm">
            <xsl:text>Generated : </xsl:text>
            <xsl:value-of select="date:month-name($now)"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="date:year($now)"/>
        </fo:block-->

        <fo:block-container absolute-position="fixed" top="100mm" left="64mm">

            <!-- Display title -->
            <fo:block space-before.minimum="100pt"
                      space-before.optimum="100pt"
                      space-before.maximum="100pt"
                      font-size="26pt"
                      font-weight="bold">
                <xsl:apply-templates select="/book/bookinfo/title"
                                     mode="title.markup"/>
            </fo:block>

            <!-- Display subtitle -->
            <fo:block space-before.minimum="21mm"
                      space-before.optimum="21mm"
                      space-before.maximum="21mm"
                      font-size="20pt"
                      font-weight="bold">
                <xsl:apply-templates select="/book/bookinfo/subtitle"
                                     mode="subtitle.markup"/>
            </fo:block>

        </fo:block-container>

        <fo:block-container absolute-position="fixed" top="180mm" left="100mm">

            <fo:block>
            <fo:external-graphic>
                <xsl:attribute name="src">
                    <xsl:if test="$img.src.path != ''">
                        <xsl:value-of select="$img.src.path"/>
                    </xsl:if>
                    <xsl:value-of select="/book/bookinfo/mediaobject/imageobject/imagedata/@fileref"/>
                </xsl:attribute>
            </fo:external-graphic>
            </fo:block>

        </fo:block-container>

    </xsl:template>

    <!-- Page with only title and subtitle between the titlepage and the
    legalnotice page -->
    <xsl:template name="book.titlepage.before.verso">
        <!-- block needed to space next block-->
        <fo:block break-before ='page'/>

        <!-- Display title -->
        <fo:block space-before.minimum="70mm"
                  space-before.optimum="70mm"
                  space-before.maximum="70mm"
                  font-size="24pt"
                  font-weight="bold"
                  margin-left="30mm">
            <xsl:apply-templates select="/book/bookinfo/title"
                                 mode="title.markup"/>
        </fo:block>

        <!-- Display subtitle -->
        <fo:block space-before.minimum="70mm"
                  space-before.optimum="70mm"
                  space-before.maximum="70mm"
                  font-size="20pt"
                  font-weight="bold"
                  margin-left="30mm">
            <xsl:apply-templates select="/book/bookinfo/subtitle" mode="subtitle.markup"/>
        </fo:block>
        
        <!-- block needed to space next block-->
        <fo:block break-before ='page'/>
    </xsl:template> <!-- End of before verso page -->

    <!-- Verso page (legalnotice) -->
    <xsl:template name="book.titlepage.verso">

        <!-- Title -->
        <fo:block font-size="20pt"
                  font-weight="bold">
            <xsl:apply-templates select="/book/bookinfo/title"
                                 mode="title.markup"/>
            <xsl:text> : </xsl:text>
            <xsl:apply-templates select="/book/bookinfo/subtitle"
                                 mode="subtitle.markup"/>
        </fo:block>

        <!-- Copyright -->
        <fo:block font-size="10pt">
            <xsl:apply-templates select="/book/bookinfo/copyright"
                                 mode="book.titlepage.verso.auto.mode"/>
        </fo:block>

        <!-- Legal notice -->
        <fo:block>
            <xsl:apply-templates select="/book/bookinfo/legalnotice"
                                 mode="titlepage.mode"/>
        </fo:block>

    </xsl:template> <!-- End of verso page -->

    <!-- Legalnotice template, taken from fo/titlepage.xsl -->
    <xsl:template match="legalnotice"
                  mode="titlepage.mode">

        <xsl:variable name="id">
            <xsl:call-template name="object.id"/>
        </xsl:variable>

        <fo:block id="{$id}">
            <xsl:if test="title">
                <fo:block font-size="14pt"
                          font-weight="bold">
                    <xsl:value-of select="title"/>
                </fo:block>
            </xsl:if>
            <xsl:apply-templates mode="titlepage.mode"/>
        </fo:block>
    </xsl:template>

 <xsl:template name="book.titlepage.separator"></xsl:template>

 <!--Chapter title page properties-->
    <xsl:attribute-set name="chapter.titlepage.recto.style">
        <xsl:attribute name="color">
            <xsl:value-of select="$chapter.title.color"/>
        </xsl:attribute>
        <xsl:attribute name="background-color">white</xsl:attribute>
        <xsl:attribute name="font-size">24pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="padding-left">1em</xsl:attribute>
        <xsl:attribute name="padding-right">1em</xsl:attribute>
    </xsl:attribute-set>

    <!--Section title page properties-->
    <xsl:attribute-set name="section.titlepage.recto.style">
        <xsl:attribute name="color">
            <xsl:value-of select="$section.title.color"/>
        </xsl:attribute>
        <xsl:attribute name="background-color">white</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
    </xsl:attribute-set>

    <!--Appendix title page properties-->
    <xsl:attribute-set name="appendix.titlepage.recto.style">
        <xsl:attribute name="color">
            <xsl:value-of select="$chapter.title.color"/>
        </xsl:attribute>
        <xsl:attribute name="background-color">white</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
    </xsl:attribute-set>

    <!--Formal titles (figure, table,...) properties-->
    <xsl:attribute-set name="formal.title.properties"
                       use-attribute-sets="normal.para.spacing">
        <xsl:attribute name="font-size">9pt</xsl:attribute>
        <xsl:attribute name="hyphenate">false</xsl:attribute>
        <xsl:attribute name="space-before.minimum">0pt</xsl:attribute>
        <xsl:attribute name="space-before.optimum">0pt</xsl:attribute>
        <xsl:attribute name="space-before.maximum">0pt</xsl:attribute>
        <xsl:attribute name="space-after.minimum">8.5pt</xsl:attribute>
        <xsl:attribute name="space-after.optimum">9pt</xsl:attribute>
        <xsl:attribute name="space-after.maximum">9.5pt</xsl:attribute>
    </xsl:attribute-set>

    <!--tips-warning-... properties-->
    <xsl:param name="admon.graphics" select="1"/>
    <xsl:param name="admon.graphics.path">
        <xsl:if test="$img.src.path != ''">
            <xsl:value-of select="$img.src.path"/>
        </xsl:if>
        <xsl:text>images/community/docbook/</xsl:text>
    </xsl:param>
    <xsl:param name="admon.graphics.extension" select="'.svg'"/>
    <xsl:attribute-set name="admonition.title.properties">
        <xsl:attribute name="font-size">13pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="hyphenate">false</xsl:attribute>
        <xsl:attribute name="keep-with-next.within-column">always
        </xsl:attribute>
    </xsl:attribute-set>

    <!--Default properties for admonitions. Color values are overridden after
    in specific template-->
    <xsl:attribute-set name="graphical.admonition.properties">
        <xsl:attribute name="color">white</xsl:attribute>
        <xsl:attribute name="background-color">#463F3D</xsl:attribute>
        <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
        <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
        <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
        <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
        <xsl:attribute name="space-after.minimum">0.8em</xsl:attribute>
        <xsl:attribute name="space-after.maximum">1em</xsl:attribute>
        <xsl:attribute name="padding-bottom">7pt</xsl:attribute>
        <xsl:attribute name="padding-top">7pt</xsl:attribute>
        <xsl:attribute name="padding-right">7pt</xsl:attribute>
        <xsl:attribute name="padding-left">7pt</xsl:attribute>
        <xsl:attribute name="margin-left">
            <xsl:value-of select="$title.margin.left"/>
        </xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="xref.properties">
        <xsl:attribute name="font-style">italic</xsl:attribute>
        <xsl:attribute name="color">#6161e1</xsl:attribute>
    </xsl:attribute-set>

    <!-- Make the section depth in the TOC 2, same as html -->
    <xsl:param name="toc.section.depth">2</xsl:param>

    <!-- Now, set enable scalefit for large images -->
    <xsl:param name="graphicsize.extension" select="'1'"/>
    <!--xsl:param name="default.image.width">15cm</xsl:param-->

    <xsl:param name="hyphenate.verbatim">1</xsl:param>
    <!--Never use hyphenation, causes guilabel, guibuttons, ... to be rendered
    badly : https://issues.apache.org/bugzilla/show_bug.cgi?id=38880 -->
    <xsl:param name="hyphenate">false</xsl:param>


    <!-- Properties for monospace and verbatim texts, should impact
    programlistings-->
    <xsl:attribute-set name="monospace.properties">
        <xsl:attribute name="font-size">1em</xsl:attribute>
        <xsl:attribute name="wrap-option">wrap</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$monospace.font.family"/>
        </xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="monospace.verbatim.properties"
                       use-attribute-sets="verbatim.properties monospace.properties">
        <xsl:attribute name="text-align">start</xsl:attribute>
        <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    </xsl:attribute-set>

    <xsl:param name="shade.verbatim" select="1"/>
    <xsl:attribute-set name="shade.verbatim.style">
        <xsl:attribute name="background-color">white</xsl:attribute>
        <xsl:attribute name="color">black</xsl:attribute>
        <xsl:attribute name="padding-left">12pt</xsl:attribute>
        <xsl:attribute name="padding-right">12pt</xsl:attribute>
        <xsl:attribute name="padding-top">0pt</xsl:attribute>
        <xsl:attribute name="padding-bottom">0pt</xsl:attribute>
        <xsl:attribute name="margin-left">
            <xsl:value-of select="$title.margin.left"/>
        </xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="verbatim.properties">
        <xsl:attribute name="space-before.minimum">0pt</xsl:attribute>
        <xsl:attribute name="space-before.optimum">0pt</xsl:attribute>
        <xsl:attribute name="space-before.maximum">0pt</xsl:attribute>
        <xsl:attribute name="space-after.minimum">8.5pt</xsl:attribute>
        <xsl:attribute name="space-after.optimum">9pt</xsl:attribute>
        <xsl:attribute name="space-after.maximum">9.5pt</xsl:attribute>
        <xsl:attribute name="hyphenate">false</xsl:attribute>
        <xsl:attribute name="wrap-option">wrap</xsl:attribute>
        <xsl:attribute name="white-space-collapse">false</xsl:attribute>
        <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
        <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
        <xsl:attribute name="text-align">start</xsl:attribute>
    </xsl:attribute-set>

    <!--Template to find component's title, used for header-->
    <xsl:template name="component.title.nomarkup">
        <xsl:param name="node" select="."/>
        <xsl:variable name="id">
            <xsl:call-template name="object.id">
                <xsl:with-param name="object" select="$node"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="title">
            <xsl:apply-templates select="$node" mode="object.title.markup">
                <xsl:with-param name="allow-anchors" select="1"/>
            </xsl:apply-templates>
        </xsl:variable>
        <xsl:copy-of select="$title"/>
    </xsl:template>

    <!--Header generation-->
    <xsl:param name="header.column.widths">2 0 1</xsl:param>

    <xsl:template name="header.content">
        <xsl:param name="pageclass" select="''"/>
        <xsl:param name="sequence" select="''"/>
        <xsl:param name="position" select="''"/>
        <xsl:param name="gentext-key" select="''"/>
        <xsl:param name="title-limit" select="'50'"/>

        <xsl:choose>
            <xsl:when test="$sequence = 'blank'">
                <!-- If blank page, put no title -->
            </xsl:when>

            <!--On left put chapter or section title truncated with...-->
            <xsl:when test="$position='left'">
                <xsl:variable name="text">
                    <xsl:call-template name="component.title.nomarkup"/>
                </xsl:variable>
                <fo:inline keep-together.within-line="always"
                           font-weight="bold">
                    <xsl:choose>
                        <xsl:when test="string-length($text) &gt; '53'">
                            <xsl:value-of
                                    select="concat(substring($text, 0, $title-limit), '...')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$text"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </fo:inline>
            </xsl:when>

            <!--Put the page number on right-->
            <xsl:when test="$position = 'right'">
                <fo:page-number/>
            </xsl:when>

            <xsl:otherwise><!--do nothing--></xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    <!--End of header generation-->

    <!--Footer generation-->
    <xsl:template name="footer.content">
        <xsl:param name="pageclass" select="''"/>
        <xsl:param name="sequence" select="''"/>
        <xsl:param name="position" select="''"/>
        <xsl:param name="gentext-key" select="''"/>
        <xsl:param name="title-limit" select="'22'"/>

        <xsl:choose>
            <!--Put the IFREMER logo in footer, on the left-->
            <xsl:when test="$position = 'left'">
                <fo:external-graphic content-height="0.3cm">
                    <xsl:attribute name="src">
                        <xsl:call-template name="fo-external-image">
                            <xsl:with-param name="filename">
                                <xsl:if test="$img.src.path != ''">
                                    <xsl:value-of select="$img.src.path"/>
                                </xsl:if>
                                <xsl:text>images/logo_NB.png</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:attribute>
                </fo:external-graphic>
            </xsl:when>

            <!-- On center put book title -->
            <xsl:when test="$position='center'">
                <fo:inline keep-together.within-line="always"
                           font-weight="bold">
                    <xsl:apply-templates select="/book/bookinfo/title"
                                         mode="title.markup"/>
                    <xsl:text> - </xsl:text>
                    <xsl:apply-templates select="/book/bookinfo/subtitle"
                                         mode="subtitle.markup"/>
                </fo:inline>
            </xsl:when>

            <!-- On right, put build date -->
            <xsl:when test="$position='right'">
                <xsl:variable name="now" select="date:date-time()"/>
                <fo:block>
                    <xsl:value-of select="date:month-name($now)"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="date:year($now)"/>
                </fo:block>

            </xsl:when>

            <xsl:when test="$sequence = 'blank'">
                <!-- If blank page, put no title -->
            </xsl:when>
            <xsl:otherwise><!--empty--></xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    <!--End of footer generation-->

    <!-- Margins (chaotic stuff, don't touch) -->
    <xsl:param name="page.margin.top">15mm</xsl:param>
    <xsl:param name="region.before.extent">10mm</xsl:param>
    <xsl:param name="body.margin.top">15mm</xsl:param>
    <xsl:param name="body.margin.bottom">15mm</xsl:param>
    <xsl:param name="region.after.extent">10mm</xsl:param>
    <xsl:param name="page.margin.bottom">15mm</xsl:param>
    <xsl:param name="page.margin.outer">30mm</xsl:param>
    <xsl:param name="page.margin.inner">30mm</xsl:param>

    <!--Allow programlistings, tables,... to be break on several columns/pages-->
    <xsl:attribute-set name="formal.object.properties">
        <xsl:attribute name="keep-together.within-column">auto</xsl:attribute>
    </xsl:attribute-set>

    <!-- Format Variable Lists as Blocks (prevents horizontal overflow). -->
    <xsl:param name="variablelist.as.blocks">1</xsl:param>

    <!-- Callouts -->
    <!-- Place callout bullets at this column in programlisting.-->
    <xsl:param name="callout.defaultcolumn">80</xsl:param>
    <xsl:param name="callout.icon.size">10pt</xsl:param>
    <!-- Use pictures instead of unicode callouts -->
    <xsl:param name="callout.unicode">0</xsl:param>
    <xsl:param name="callout.graphics">1</xsl:param>
    <xsl:param name="callout.graphics.path">
        <xsl:if test="$img.src.path != ''">
            <xsl:value-of select="$img.src.path"/>
        </xsl:if>
        <xsl:text>images/community/docbook/callouts/</xsl:text>
    </xsl:param>
    <xsl:param name="callout.graphics.extension">.svg</xsl:param>

    <!-- For indexes -->
    <xsl:attribute-set name="xep.index.item.properties"
                       use-attribute-sets="index.page.number.properties">
        <xsl:attribute name="merge-subsequent-page-numbers">true</xsl:attribute>
        <xsl:attribute name="link-back">true</xsl:attribute>
    </xsl:attribute-set>

    <!-- Override indexterm template to avoid title not being with following
     paragraph when putting indexterm right after title-->

    <xsl:template name="indexterm">
        <xsl:variable name="id">
            <xsl:call-template name="object.id"/>
        </xsl:variable>
        <fo:block keep-with-previous.within-column="always"
                  keep-with-next.within-column="always">
            <xsl:attribute name="id">
                <xsl:value-of select="$id"/>
            </xsl:attribute>
        </fo:block>
    </xsl:template>

    <!-- Put an arrow between guimenus and guimenuitems in menuchoices-->
    <xsl:param name="menuchoice.menu.separator">
        <fo:inline font-family="Symbol"
                   keep-with-next.within-line="always"
                   keep-with-previous.within-line="always"
                   wrap-option="no-wrap">&#xA0;&#x2192;&#xA0;</fo:inline>
    </xsl:param>

    <!-- Allow figures to contains several screenshots, all put on the same
    line -->
    <!--xsl:attribute-set name="figure.properties">
        <xsl:attribute name="space-before.minimum">0.5em</xsl:attribute>
        <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
        <xsl:attribute name="space-before.maximum">2em</xsl:attribute>
        <xsl:attribute name="space-after.minimum">0.5em</xsl:attribute>
        <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
        <xsl:attribute name="space-after.maximum">2em</xsl:attribute>
        <xsl:attribute name="keep-together.within-line">always</xsl:attribute>
    </xsl:attribute-set-->

    <!-- Need this to put special characters in docbook -->
    <!-- To use it, place your special character between
                 <symbol role='symbolFont'></symbol> tags.-->
    <xsl:template match="symbol [@role = 'symbolFont']">
        <fo:inline font-family="Symbol">
            <xsl:call-template name="inline.charseq"/>
        </fo:inline>
    </xsl:template>

    <!-- Need this to put callouts in programlisting on the right place -->
    <xsl:template match="programlisting/co">
        <xsl:apply-templates select="." mode="callout-bug">
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template name="callout-bug">
        <xsl:param name="conum" select='1'/>
        <fo:external-graphic
                src="{$callout.graphics.path}{$conum}{$callout.graphics.extension}"
                id="{@id}"
                content-width="{$callout.icon.size}"
                width="{$callout.icon.size}"/>
    </xsl:template>

    <!-- Display guibuttons as real buttons -->
    <xsl:template match="guibutton">
        <fo:inline border="2px outset #dddddd"
                   color="black"
                   background-color="#dddddd"
                   keep-together.within-line="always"
                   hyphenate="false">
            <xsl:call-template name="inline.charseq"/>
        </fo:inline>
    </xsl:template>

    <!-- Display guilabel capitalized -->
    <xsl:template match="guilabel">
        <fo:inline border="1px solid #000000"
                   text-transform="uppercase"
                   padding-top="2px"
                   padding-left="2px"
                   padding-right="2px"
                   keep-together.within-line="always"
                   hyphenate="false">
            <xsl:call-template name="inline.charseq"/>
        </fo:inline>
    </xsl:template>

    <!-- Display guimenu capitalized -->
    <xsl:template match="guimenu">
        <fo:inline border="1px solid #000000"
                   text-transform="uppercase"
                   padding-top="2px"
                   padding-left="2px"
                   padding-right="2px"
                   keep-together.within-line="always"
                   keep-with-next.within-line="always"
                   wrap-option="no-wrap">
            <xsl:call-template name="inline.charseq"/>
        </fo:inline>
    </xsl:template>

    <!-- Display guisubmenu capitalized -->
    <xsl:template match="guisubmenu">
        <fo:inline border="1px solid #000000"
                   text-transform="uppercase"
                   padding-top="2px"
                   padding-left="2px"
                   padding-right="2px"
                   keep-together.within-line="always"
                   keep-with-next.within-line="always"
                   keep-with-previous.within-line="always"
                wrap-option="no-wrap">
            <xsl:call-template name="inline.charseq"/>
        </fo:inline>
    </xsl:template>

    <!-- Display guimenuitem capitalized -->
    <xsl:template match="guimenuitem">
        <fo:inline border="1px solid #000000"
                   text-transform="uppercase"
                   padding-top="2px"
                   padding-left="2px"
                   padding-right="2px"
                   keep-together.within-line="always"
                   keep-with-next.within-line="always"
                   keep-with-previous.within-line="always"
                   wrap-option="no-wrap">
            <xsl:call-template name="inline.charseq"/>
        </fo:inline>
    </xsl:template>

    <!-- Display guiicon smallsize -->
    <xsl:template match="guiicon">
        
        <xsl:variable name="src">
            <xsl:if test="inlinemediaobject">
                <xsl:value-of select="inlinemediaobject/imageobject/imagedata/@fileref"/>
            </xsl:if>
            <!--Should be useless, inlinegraphic is deprecated-->
            <xsl:if test="inlinegraphic">
                <xsl:value-of select="inlinegraphic/@fileref"/>
            </xsl:if>
        </xsl:variable>
        
        <fo:external-graphic width="auto" height="1em"
        content-height="1em">
        <xsl:attribute name="src">
            <xsl:value-of select="$src"/>
        </xsl:attribute>
    </fo:external-graphic>
    </xsl:template>

    <!--Override para template to put the line-stacking-strategy to font-height.
    Will keep the space between line constant, whatever the inline graphic size
    is-->
    <xsl:template match="para">
        <xsl:variable name="keep.together">
            <xsl:call-template name="pi.dbfo_keep-together"/>
        </xsl:variable>
        <fo:block xsl:use-attribute-sets="normal.para.spacing"
                  line-stacking-strategy="font-height">
            <xsl:if test="$keep.together != ''">
                <xsl:attribute name="keep-together.within-column">
                    <xsl:value-of
                            select="$keep.together"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:call-template name="anchor"/>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>


    <!-- Note admonition -->
    <xsl:template match="note">
        <xsl:variable name="id">
            <xsl:call-template name="object.id"/>
        </xsl:variable>
        <xsl:variable name="graphic.width">
            <xsl:apply-templates select="." mode="admon.graphic.width"/>
        </xsl:variable>

        <fo:block id="{$id}"
                  color="#4c5253"
                  background-color="#b5bcbd"
                  xsl:use-attribute-sets="graphical.admonition.properties">
            <fo:list-block
                    provisional-distance-between-starts="{$graphic.width} + 18pt"
                    provisional-label-separation="18pt">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:external-graphic width="auto" height="auto"
                                                 content-width="{$graphic.width}">
                                <xsl:attribute name="src">
                                    <xsl:call-template name="admon.graphic"/>
                                </xsl:attribute>
                            </fo:external-graphic>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <xsl:if test="$admon.textlabel != 0 or title or info/title">
                            <fo:block color="#4c5253"
                                      xsl:use-attribute-sets="admonition.title.properties">
                                <xsl:apply-templates select="."
                                                     mode="object.title.markup"/>
                            </fo:block>
                        </xsl:if>
                        <fo:block
                                xsl:use-attribute-sets="admonition.properties">
                            <xsl:apply-templates/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:block>
    </xsl:template>

    <!-- Tip admonition -->
    <xsl:template match="tip">
        <xsl:variable name="id">
            <xsl:call-template name="object.id"/>
        </xsl:variable>
        <xsl:variable name="graphic.width">
            <xsl:apply-templates select="." mode="admon.graphic.width"/>
        </xsl:variable>

        <fo:block id="{$id}"
                  color="#4c5253"
                  background-color="#fef0a3"
                  xsl:use-attribute-sets="graphical.admonition.properties">
            <fo:list-block
                    provisional-distance-between-starts="{$graphic.width} + 18pt"
                    provisional-label-separation="18pt">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:external-graphic width="auto" height="auto"
                                                 content-width="{$graphic.width}">
                                <xsl:attribute name="src">
                                    <xsl:call-template name="admon.graphic"/>
                                </xsl:attribute>
                            </fo:external-graphic>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <xsl:if test="$admon.textlabel != 0 or title or info/title">
                            <fo:block color="#4c5253"
                                      xsl:use-attribute-sets="admonition.title.properties">
                                <xsl:apply-templates select="."
                                                     mode="object.title.markup"/>
                            </fo:block>
                        </xsl:if>
                        <fo:block
                                xsl:use-attribute-sets="admonition.properties">
                            <xsl:apply-templates/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:block>
    </xsl:template>

    <!-- Warning admonition -->
    <xsl:template match="warning">
        <xsl:variable name="id">
            <xsl:call-template name="object.id"/>
        </xsl:variable>
        <xsl:variable name="graphic.width">
            <xsl:apply-templates select="." mode="admon.graphic.width"/>
        </xsl:variable>

        <fo:block id="{$id}"
                  color="white"
                  background-color="#7b1e1e"
                  xsl:use-attribute-sets="graphical.admonition.properties">
            <fo:list-block
                    provisional-distance-between-starts="{$graphic.width} + 18pt"
                    provisional-label-separation="18pt">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:external-graphic width="auto" height="auto"
                                                 content-width="{$graphic.width}">
                                <xsl:attribute name="src">
                                    <xsl:call-template name="admon.graphic"/>
                                </xsl:attribute>
                            </fo:external-graphic>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <xsl:if test="$admon.textlabel != 0 or title or info/title">
                            <fo:block xsl:use-attribute-sets="admonition.title.properties">
                                <xsl:apply-templates select="."
                                                     mode="object.title.markup"/>
                            </fo:block>
                        </xsl:if>
                        <fo:block
                                xsl:use-attribute-sets="admonition.properties">
                            <xsl:apply-templates/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:block>
    </xsl:template>

    <!-- Warning admonition -->
    <xsl:template match="caution">
        <xsl:variable name="id">
            <xsl:call-template name="object.id"/>
        </xsl:variable>
        <xsl:variable name="graphic.width">
            <xsl:apply-templates select="." mode="admon.graphic.width"/>
        </xsl:variable>

        <fo:block id="{$id}"
                  color="white"
                  background-color="#C35617"
                  xsl:use-attribute-sets="graphical.admonition.properties">
            <fo:list-block
                    provisional-distance-between-starts="{$graphic.width} + 18pt"
                    provisional-label-separation="18pt">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block>
                            <fo:external-graphic width="auto" height="auto"
                                                 content-width="{$graphic.width}">
                                <xsl:attribute name="src">
                                    <xsl:call-template name="admon.graphic"/>
                                </xsl:attribute>
                            </fo:external-graphic>
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <xsl:if test="$admon.textlabel != 0 or title or info/title">
                            <fo:block
                                    xsl:use-attribute-sets="admonition.title.properties">
                                <xsl:apply-templates select="."
                                                     mode="object.title.markup"/>
                            </fo:block>
                        </xsl:if>
                        <fo:block
                                xsl:use-attribute-sets="admonition.properties">
                            <xsl:apply-templates/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:block>
    </xsl:template>

    <!-- Display acronyms in glossary -->
    <xsl:param name="glossentry.show.acronym">yes</xsl:param>

    <!-- Change footnote template -->
    <xsl:template match="footnote">
        <xsl:choose>
            <xsl:when test="ancestor::table or ancestor::informaltable">
                <xsl:call-template name="format.footnote.mark">
                    <xsl:with-param name="mark">
                        <xsl:apply-templates select="." mode="footnote.number"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <fo:footnote>
                    <fo:inline>
                        <xsl:call-template name="format.footnote.mark">
                            <xsl:with-param name="mark">
                                <xsl:apply-templates select="."
                                                     mode="footnote.number"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </fo:inline>
                    <fo:footnote-body
                            xsl:use-attribute-sets="footnote.properties">

                        <!-- Modifications here from original template -->
                        <fo:block>
                            <fo:inline font-size="0.7em" >
                                <xsl:apply-templates select="."
                                                 mode="footnote.number"/>
                            </fo:inline>
                            <xsl:text> </xsl:text>
                            <xsl:apply-templates mode="footnote"/>
                        </fo:block>
                        <!-- end of modifications -->
                    </fo:footnote-body>
                </fo:footnote>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="para" mode="footnote">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

</xsl:stylesheet>
