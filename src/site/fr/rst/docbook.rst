.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Docbook quoi qu'est-ce ?
========================

Docbook est un langage de rédaction de documentation structuré, basé sur XML, et
dont l'objectif est de décorréler le fond de la forme. Ce format de
documentation est particulièrement adapté à la documentation logicielle car
il permet, en se basant sur une seule source commune, de créer plusieurs format
de sortie de grande qualité. Il est, de part son approche, assez proche de
Latex.

Ainsi, toute la documentation est tappée 'au kilomètre' dans des fichiers
docbook. Avec différentes balises xml permettant d'identifier images,
paragraphes, chapitres, sections, ...

Exemple de fichier docbook :

  <?xml version="1.0" encoding="UTF-8" ?>
  <!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
    "/usr/share/xml/docbook/schema/dtd/4.4/docbookx.dtd">

  <book>

    <bookinfo>
      <title>Bonjour le monde</title>
    </bookinfo>

    <chapter>
      <title>Bonjour le monde</title>

      <para>Ceci est mon  premier fichier docbook.</para>

    </chapter>
  </book>

Les ficheirs docbook sont ensuites transformées vers différents formats de
sortie (html, pdf, manpage...)

Dans cette documentation, je ne vais pas rentrer dans le détail de toutes les
(nombreuses) balises docbook, de nombreux sites le font très bien. Je vais
plutot détailler les ressource essentielles permettant d'appréhender rapidement
le format docbook et ainsi faire évoluer la documentation de TNPC.

Docbook: The Definitive Guide (anglais)
------------------------------------------

Livre de référence en version éléctronique, rédigé par Norman Walsh, créateur de
docbook, Leonard Muellner et Bob Stayton, principaux contributeurs au format :
http://www.docbook.org/tdg/en/html/docbook.html

Ce livre explique docbook ainsi que toutes les balises du format.

Docbook element quick reference card (anglais)
----------------------------------------------

Liste des balises les plus courramment utilisées par usage :
http://www.docbook.org/tdg/en/html/docbook.html



Carte de référence docbook (français)
-------------------------------------

Liste en français, accompagnée d'examples, des principales balises utilisées :
http://feloy.free.fr/dbrefcard.pdf
