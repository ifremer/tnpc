.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Get the documentation sources
=============================

The sources are stored in a subversion repository you can find here :
https://labs.libre-entreprise.org/scm/?group_id=160

I assume you know how to deal with subversion. For people that do not know how
to deal with subversion, a tutorial to use the fusion forge subversion
repository on Windows is downloadbale here :
http://all4dev.libre-entreprise.org/data/FusionForge.pdf

It is advised to create an account on the forge, as it is needed to
make changes to the documentation (to avoid spam on the documentation).