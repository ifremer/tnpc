.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Change the way documentation is generated
=========================================

You might want to change the way the documentation is generated for, for
example, change the way legends are displayed on pictures, how the titlepage
looks like...

This process is a bit tedious, but to do so, you can change the customisation
layers located in the xsl directory.

To understand how to change those customization layers, the best way is to
refer to Bob Stayton's book : 'Docbook XSL : the Complete Guide' :
http://www.sagehill.net/docbookxsl/