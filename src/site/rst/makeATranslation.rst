.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Make a translation of the documentation
=======================================

Everything have been set for the French translation, you only have to copy the
files form doc/src/docbkx/en-US in doc/src/docbkx/fr-FR, rename master.docbook
to master-fr.docbokk and then open the different files and translate them.

Then generate the documentation and you got your language generated files along
with the english ones.

The HTML documentation will be broken (no images). Uncomment the right part
in the pom.xml file (the copy part in the generate-html-doc execution of the
maven docbkx plugin).

Translate to another language than French
-----------------------------------------

To translate the documentation to another language than French, you have to make
some modifications on the pom.xml file (the maven file describing how to build
the documentation). First, copy the content of the create-pdf-docs-fr execution
of the docbkx maven plugin and adapt it to your language. Example for Spanish
language ::

  <execution>
    <id>create-pdf-docs-es</id>
    <goals>
      <goal>generate-pdf</goal>
    </goals>
    <configuration>
      <sourceDirectory>${basedir}/src/docbkx/es-ES</sourceDirectory>

      <imgSrcPath>${project.basedir}/target/classes/</imgSrcPath>

      <l10nGentextDefaultLanguage>es-ES</l10nGentextDefaultLanguage>

      <foCustomization>
        ${project.basedir}/target/classes/pdf.xsl
      </foCustomization>

      <postProcess>
        <copy todir="target/pdf/es-ES">
          <fileset dir="target/docbkx/pdf"
                   includes="master-es.doc.pdf"/>
        </copy>
      </postProcess>
    </configuration>

    <phase>compile</phase>
  </execution>

You also have to add the copy tasks for your language in the create-html-docs
execution of the maven docbkx plugin. Example for Spanish language ::

  <!-- For spanish doc-->
  <copy todir="target/docbkx/html/es-ES/images">
    <fileset dir="src/docbkx/es-ES/images"/>
  </copy>
  <copy todir="target/docbkx/html/es-ES/css">
    <fileset dir="target/classes/css"/>
  </copy>
  <copy todir="target/docbkx/html/es-ES/images/community">
    <fileset dir="target/classes/images/community"/>
  </copy>
  <copy todir="target/docbkx/html/es-ES/images/logo">
    <fileset dir="target/classes/images/logo"/>
  </copy>
  <copy todir="target/docbkx/html/es-ES/images/">
    <fileset dir="target/classes/images/favicon"/>
  </copy>

Then, do the same than for French documentation
