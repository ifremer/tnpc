.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
How to build the documentation from the sources
===============================================

Once you get the sources, you might get interested in generating the PDF or HTML
documentation from the sources.

To build the documentation, we use Maven (it manage the dependencies, the build
process, this website...). To build the documentation, you must install Maven on
your computer. To do so, please refer to the Maven documentation :
http://maven.apache.org/

(Note that everything is done from the command line).

When Maven is ready, launch the 'mvn clean package' command from the
directory where you download the sources (where you got the pom.xml file and the
doc, style or xsl directories.

You should see a "Build successful" message and get the built documentation
on the doc/target/docbkx directory

If the build failed, do not hesitate to contact us on the mailing-lists.