.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Manage callouts on screenshots
==============================

To do professional documentation, it is really useful to have callouts drown
onto your screenshots and reference made into the text instead of red boxes with
arrow and text. First this allows you to change the text without changing the
screenshot. Secondly, you can change the screenshot without changing the text.

But to do so, everything is not yet automatic using Maven, we need to use some
external tools, provided by Norman Walsh : areaoverlay and areasearch.

Areasearch is a python script that looks for small boxes and give its coordinates.
Areaoverlay inspect a docbook file and insert callout bugs on images at given
coordinates.

The combination of the two methods will allow us to generate nice screenshots
with callout bugs.

First of all, you will need Gimp software : http://www.gimp.org/. then follow
Norman Walsh tutorial here : http://norman.walsh.name/2006/06/10/imageobjectco

For windows users, go get python here : http://www.python.org/download/windows/
if you do not have it.

We are trying to create a maven plugin that does it, but it is not yet ready, so
we have to go through this not really immediate step for the moment.