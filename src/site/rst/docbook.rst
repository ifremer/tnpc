.. -
.. * #%L
.. * TNPC documentation :: Parent pom
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2010 - 2011 Ifremer, CodeLutin, Noesis
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Docbook what is this ?
======================

Docbook is a structured documentation writing language, based on XML, and whose
objective is to separate the content from the rendering. This documentation
format is well adapted to software documentation (which it was aimed for at the
beginning) as it allow to create several output formt of high quality from one
only source. This language is, by its approach, quite close to Latex.

So all the documentation is typed in bulkly in docbook files with differents xml
tags allowing to identify screenshots, paragraphs, chapters, sections...

Docbook file example ::

  <?xml version="1.0" encoding="UTF-8" ?>
  <!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
    "/usr/share/xml/docbook/schema/dtd/4.4/docbookx.dtd">

  <book>

    <bookinfo>
      <title>Hello, world</title>
    </bookinfo>

    <chapter>
      <title>Hello, world</title>

      <para>This is my first DocBook file.</para>

    </chapter>
  </book>

The docbook files are then transformed to different output formats (html, pdf,
manpage...)

In this documentation, I will not enter in the detail of all the (numerous)
docbook tags, a lot of web sites do it pretty well. I will focus on linking to
the essential resources allowing to learn docbook quickly and so make TNPC
documentation evolve.

Docbook: The Definitive Guide
-----------------------------

Reference book about docbook, written by Norman Walsh, docbook creator, Leonard
Muellner and Bob Stayton, docbook contributors. This book explains docbook and
all the different tags : http://www.docbook.org/tdg/en/html/docbook.html

Docbook element quick reference card
------------------------------------

List, to be printed, of the most used tags per usage :
http://www.docbook.org/tdg/en/html/docbook.html

Docbook Tutorials
-----------------

List of docbook tutorials in several languages, some might be quite old :
http://wiki.docbook.org/topic/DocBookTutorials

Carte de référence docbook (French)
-----------------------------------

List, in French, of the most used tags, with examples. To be printed :
http://feloy.free.fr/dbrefcard.pdf